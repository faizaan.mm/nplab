#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
#include <ctype.h>
void  main()
{
    int s,r,recb,sntb,x,i;
    int sum =0;
    struct sockaddr_in server;
    char buff[50];
    char buff1[50];

    s=socket(AF_INET,SOCK_STREAM,0);
    if(s==-1)
    {
        printf("\nSocket creation error");
        exit(0);
    }
    server.sin_family=AF_INET;
    server.sin_port=htons(8080);
    server.sin_addr.s_addr=inet_addr("127.0.0.1");
    r=connect(s,(struct sockaddr*)&server,sizeof(server));
    if(r==-1)
    {
        printf("\nConnection error");
        exit(0);
    }

    recb=recv(s,buff,sizeof(buff),0);  

    if(recb==-1)    
        printf("!!");

    for(i=1;i<strlen(buff);i++)
        {
            if(buff[i]>='0'&&buff[i]<='9')
            {
                sum+= (int)buff[i]-48;
            }
            else if(tolower(buff[i])>='a'&&buff[i]<='z')
            {
                sum+= (int)tolower(buff[i])-96;
            }
        }

    if(buff[0]=='1')
    {
        sprintf(buff1, "%d", sum);
        strcat(buff1," - Client 1\n");
        sntb=send(s,buff1,sizeof(buff1),0);
    }
    if(buff[0]=='2')
    {
        if(sum%2==0)
        {
            strcpy(buff1,"Even - Client 2");
        }
        else{
            strcpy(buff1,"Odd - Client 2");
        }
       sntb=send(s,buff1,sizeof(buff1),0);
    }       
    close(s);
}