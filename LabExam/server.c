#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void main()
{
	int s,r,recb,sntb,x,ns,a=0,i=0, cons =0, status=0;
	socklen_t len;
	struct sockaddr_in server,client;
	char buff[50];
	char buff1[50];
	s=socket(AF_INET,SOCK_STREAM,0);
	if(s==-1)
	{

		printf("\nSocket creation error.");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8080);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));

	if(r==-1)
	{
		printf("\nBinding error.");
		exit(0);
	}

	r=listen(s,2);

	if(r==-1)
	{
		close(s);
		exit(0);
	}

	printf("Enter Text\n");
   	scanf("%[^\n]s",buff);
	while(1)
	{
		len=sizeof(client);
		ns=accept(s,(struct sockaddr*)&client, &len);
		if(ns==-1)
		{
			close(s);
			exit(0);
		}
		cons++;
		if(cons<=2)
		{
			pid_t wpid;
			pid_t pid=fork();
			if(pid==0)
			{	
				sprintf(buff1, "%d", cons);
				strcat(buff1,buff);
    			sntb=send(ns,buff1,sizeof(buff1),0);
    			recb=recv(ns,buff1,sizeof(buff1),0);
    			printf(buff1);	
				break;
				close(s);
			}
			while ((wpid = wait(&status)) > 0);
		}
		else
		{
			printf("Terminating\n");
			close(s);
			close(ns);
		}
	}	
}