set val(chan)   Channel/WirelessChannel
set val(prop)   Propagation/TwoRayGround
set val(ant)    Antenna/OmniAntenna
set val(ll)     LL
set val(ifq)    Queue/DropTail/PriQueue
set val(ifqlen) 50
set val(netif)  Phy/WirelessPhy
set val(mac)    Mac/802_11
set val(rp)     AODV
set val(nn)     3
set val(x)      500
set val(y)      500
set val(stop)   150

set ns [new Simulator]
set tracefd [open out.tr w]
set nf [open out.nam w]

$ns trace-all $tracefd
$ns namtrace-all-wireless $nf $val(x) $val(y)

proc finish {} {
	global ns nf tracefd
	$ns flush-trace
	close $nf
	close $tracefd
	exec nam out.nam &
	exit 0
}

set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)

create-god $val(nn)

$ns node-config \
  -adhocRouting $val(rp) \
  -llType $val(ll) \
  -macType $val(mac) \
  -ifqType $val(ifq) \
  -ifqLen $val(ifqlen) \
  -antType $val(ant) \
  -propType $val(prop) \
  -phyType $val(netif) \
  -channelType $val(chan) \
  -topoInstance $topo \
  -agentTrace ON \
  -routerTrace ON \
  -macTrace OFF \
  -movementTrace ON

set n(0) [$ns node]
set n(1) [$ns node]
set n(2) [$ns node]	

$n(0) set X_ 5.0
$n(0) set Y_ 5.0
$n(0) set Z_ 0.0

$n(1) set X_ 400.0
$n(1) set Y_ 400.0
$n(1) set Z_ 0.0

$n(2) set X_ 200.0
$n(2) set Y_ 200.0
$n(2) set Z_ 0.0

$ns at 10.0 "$n(0) setdest 400.0 400.0 5.0"
$ns at 15.0 "$n(1) setdest 5.0 5.0 5.0"

set tcp [new Agent/TCP/Newreno]
set sink [new Agent/TCPSink]
$ns attach-agent $n(0) $tcp
$ns attach-agent $n(1) $sink
$ns connect $tcp $sink

set ftp0 [new Application/FTP]
$ftp0 attach-agent $tcp
$ns at 10.0 "$ftp0 start"

for { set i 0 } { $i < $val(nn) } { incr i } {
  $ns initial_node_pos $n($i) 30
  $ns at $val(stop) "$n($i) reset";
}

$ns at $val(stop) "$ns nam-end-wireless $val(stop)"
$ns at $val(stop) "finish"
$ns at 150.01 "$ns halt"

$ns run