#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void main()
{
	int s,r,recb,sntb,x,ns,a=0,i=0, cons =0, status=0;
	socklen_t len;
	struct sockaddr_in server,client;
	char buff[50];
	s=socket(AF_INET,SOCK_STREAM,0);
	if(s==-1)
	{

		printf("\nSocket creation error.");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8080);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));

	if(r==-1)
	{
		printf("\nBinding error.");
		exit(0);
	}

	r=listen(s,2);

	if(r==-1)
	{
		close(s);
		exit(0);
	}


	FILE *f;
	FILE *f1;
	f=fopen("file.txt","a");
	while(1)
	{
		len=sizeof(client);
		ns=accept(s,(struct sockaddr*)&client, &len);
		if(ns==-1)
		{
			close(s);
			exit(0);
		}
		cons++;
		if(cons<=2)
		{
			pid_t wpid;
			pid_t pid=fork();
			if(pid==0)
			{
				printf("Client %d IP address-%s\n",cons, inet_ntoa(client.sin_addr));
				printf("Port is-%d\n", (int) ntohs(client.sin_port));
				close(s);
				recb=recv(ns,buff,sizeof(buff),0);		
				fprintf(f," %s",buff);
				fclose(f);
				break;
			}
			while ((wpid = wait(&status)) > 0);
			if(cons>=2)
			{
				f1 = fopen("file.txt","r");
				fscanf(f1, "%[^\n]", buff);
				printf("%s\n",buff);
				fclose(f1);		
			}
		}
		else
		{
			printf("Terminating\n");
			close(s);
			close(ns);
		}
	}	
}