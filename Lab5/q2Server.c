#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<time.h>
void sort(char*);
void main()
{
	int s,r,recb,sntb,x,ns,cons=0,i=0;
	socklen_t len;
	time_t now;
	struct sockaddr_in server,client;
	char buff[50];
	char buff1[50];
	char result[50];
	s=socket(AF_INET,SOCK_STREAM,0);
	if(s==-1)
	{

		printf("\nSocket creation error.");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8080);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));

	if(r==-1)
	{
		printf("\nBinding error.");
		exit(0);
	}

	r=listen(s,2);

	if(r==-1)
	{
		close(s);
		exit(0);
	}

	while(1)
	{
		len=sizeof(client);
		ns=accept(s,(struct sockaddr*)&client, &len);
		cons++;
		time(&now);
		if(ns==-1)
		{
			close(s);
			exit(0);
		}
		if (recv(ns,buff,sizeof(buff),0) == -1) 
		{
            printf("\n Error Reading Data from Socket.\n");
            close(ns);
            close(s);
        }        

		if (recv(ns,buff1,sizeof(buff1),0) == -1) 
		{
            printf("\n Error Reading Data from Socket.\n");
            close(ns);
            close(s);
        }        

		sort(buff);
		sort(buff1);

		printf("Client %d IP address-%s\n",cons, inet_ntoa(client.sin_addr));
		printf("Port is-%d\n", (int) ntohs(client.sin_port));
		printf("Time-%s\n",ctime(&now));
				
		if(strcmp(buff,buff1)==0)
		{
			strcpy(result,"Anagrams");
			sntb=send(ns,result,sizeof(result),0);
		}
		else
		{
			strcpy(result,"Not Anagrams");
			sntb=send(ns,result,sizeof(result),0);
		}
	}	
}
void sort(char *str)
{
    int i;
    for (i = 0; i < strlen(str) - 1; i++)
        for (int j = 0; j < strlen(str) - i - 1; j++)
            if (str[j + 1] < str[j]) 
            {
                char t = str[j];
                str[j] = str[j + 1];
                str[j + 1] = t;
            }
}