#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<stdlib.h>
#include<stdio.h>
void main()
{
	int s,r,recb,sntb,x;
	int ca;

	socklen_t len;
	struct sockaddr_in server,client;
	char buff[50];

	s=socket(AF_INET,SOCK_DGRAM,0);
	if(s==-1)
	{
		printf("\nSocket creation error");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8080);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	len=sizeof(client);
	ca=sizeof(client);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));

	if(r==-1)
	{
		printf("\nBinding error");
		exit(0);
	}

	while(1)
	{
		recb=recvfrom(s,buff,sizeof(buff),0,(struct sockaddr*)&client,&ca);
	
		if(strcmp(buff,"halt")==0)
			break;

		int k=0;
		int l=strlen(buff);
		int j=l-1;
		int flag=1;
		char result[100];
		char s1[100];
		strcpy(result,"");

		while(k<l/2)
		{
			if(buff[k]!=buff[j])
			{
				flag=0;
				break;
			}
			k++;
			j--;
		}
		if(flag==1)
		{	
			strcat(result,"Palindrome \n Length=");
			char s1[10];
			sprintf(s1,"%d",l);
			strcat(result,s1);
		}
		else
		{
			strcat(result,"Not Palindrome \n Length=");
			sprintf(s1,"%d",l);
			strcat(result,s1);
		}

		int a=0;
		int e=0;
		int i=0;
		int o=0;
		int u=0;

		for(k=0;k<l;k++)
		{
	
			if(buff[k]=='a')
				a++;
			if(buff[k]=='e')
				e++;
			if(buff[k]=='i')
				i++;
			if(buff[k]=='o')
				o++;
			if(buff[k]=='u')			
				u++;			
		}
		strcpy(s1,"");

		sprintf(s1," a= %d\t",a);
		strcat(result,s1);

		sprintf(s1," e= %d\t",e);
		strcat(result,s1);
		
		sprintf(s1," i= %d\t",i);
		strcat(result,s1);
		
		sprintf(s1," o= %d\t",o);
		strcat(result,s1);
		
		sprintf(s1," u= %d\t",u);
		strcat(result,s1);
	
		sntb=sendto(s,result,sizeof(result),0,(struct sockaddr*)&client,len);
	}
}