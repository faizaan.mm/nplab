#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void  main()
{
    int s,r,recb,sntb,x;

    struct sockaddr_in server;
    char buff[50];

    s=socket(AF_INET,SOCK_STREAM,0);
    if(s==-1)
    {
        printf("\nSocket creation error");
        exit(0);
    }
    server.sin_family=AF_INET;
    server.sin_port=htons(8080);
    server.sin_addr.s_addr=inet_addr("127.0.0.1");
    r=connect(s,(struct sockaddr*)&server,sizeof(server));
    if(r==-1)
    {
        printf("\nConnection error");
        exit(0);
    }
    printf("\n");   
    int n;
    printf("\nEnter length \n");
    scanf("%d",&n);
    sprintf(buff, "%d", n);
    sntb=send(s,buff,sizeof(buff),0);
    int i;
    int *arr=(int *)malloc(sizeof(int)*n);
    printf("\nEnter elements\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",arr+i);
    }
    sntb=send(s,arr,sizeof(int)*n,0);
    while(1)
    {
        printf("1.Sort \n 2.Search \n 3.Split \n 4.Exit\n");
        int opt;    
        scanf("%d",&opt);
        sprintf(buff, "%d", opt);
        sntb=send(s,buff,sizeof(buff),0);
        if(opt==2)
        {
            printf("Enter Search element");
            int ele;
            scanf("%d",&ele);
            sprintf(buff, "%d", ele);
            sntb=send(s,buff,sizeof(buff),0);
        }    
        if(opt==4)
        {
            printf("Exit");
            break;
        }
    }
}