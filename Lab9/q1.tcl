set val(chan)   Channel/WirelessChannel
set val(prop)   Propagation/TwoRayGround
set val(ant)    Antenna/OmniAntenna
set val(ll)     LL
set val(ifq)    Queue/DropTail/PriQueue
set val(ifqlen) 50
set val(netif)  Phy/WirelessPhy
set val(mac)    Mac/802_11
set val(rp)     AODV
set val(nn)     10
set val(x)      500
set val(y)      500
set val(stop)   150

set ns [new Simulator]
set tracefd [open out.tr w]
set nf [open out.nam w]

$ns trace-all $tracefd
$ns namtrace-all-wireless $nf $val(x) $val(y)

proc finish {} {
	global ns nf tracefd
	$ns flush-trace
	close $nf
	close $tracefd
	exec nam out.nam &
	exit 0
}

set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)

create-god $val(nn)

$ns node-config \
  -adhocRouting $val(rp) \
  -llType $val(ll) \
  -macType $val(mac) \
  -ifqType $val(ifq) \
  -ifqLen $val(ifqlen) \
  -antType $val(ant) \
  -propType $val(prop) \
  -phyType $val(netif) \
  -channelType $val(chan) \
  -topoInstance $topo \
  -agentTrace ON \
  -routerTrace ON \
  -macTrace OFF \
  -movementTrace ON

set n(0) [$ns node]
set n(1) [$ns node]
set n(2) [$ns node]
set n(3) [$ns node]
set n(4) [$ns node]
set n(5) [$ns node]	
set n(6) [$ns node]
set n(7) [$ns node]
set n(8) [$ns node]
set n(9) [$ns node]

$n(0) set X_ 300.0
$n(0) set Y_ 500.0
$n(0) set Z_ 0.0

$n(1) set X_ 150.0
$n(1) set Y_ 300.0
$n(1) set Z_ 0.0

$n(2) set X_ 450.0
$n(2) set Y_ 300.0
$n(2) set Z_ 0.0

$n(3) set X_ 300.0
$n(3) set Y_ 150.0
$n(3) set Z_ 0.0

$n(4) set X_ 200.0
$n(4) set Y_ 100.0
$n(4) set Z_ 0.0

$n(5) set X_ 100.0
$n(5) set Y_ 200.0
$n(5) set Z_ 0.0

$n(6) set X_ 450.0
$n(6) set Y_ 350.0
$n(6) set Z_ 0.0

$n(7) set X_ 300.0
$n(7) set Y_ 450.0
$n(7) set Z_ 0.0

$n(8) set X_ 450.0
$n(8) set Y_ 400.0
$n(8) set Z_ 0.0

$n(9) set X_ 500.0
$n(9) set Y_ 300.0
$n(9) set Z_ 0.0

set tcp [new Agent/TCP/Newreno]
set sink [new Agent/TCPSink]
$ns attach-agent $n(0) $tcp
$ns attach-agent $n(1) $sink
$ns connect $tcp $sink
set ftp0 [new Application/FTP]
$ftp0 attach-agent $tcp
$ns at 10.0 "$ftp0 start"

set udp [new Agent/UDP]
set null [new Agent/Null]
$ns attach-agent $n(0) $udp
$ns attach-agent $n(2) $null
$ns connect $udp $null
set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$ns at 10.0 "$cbr start"

for { set i 0 } { $i < $val(nn) } { incr i } {
  $ns initial_node_pos $n($i) 30
  $ns at $val(stop) "$n($i) reset";
}

$ns at $val(stop) "$ns nam-end-wireless $val(stop)"
$ns at $val(stop) "finish"
$ns at 150.01 "$ns halt"

$ns run