#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void main()
{
    int s,r,recb,sntb,x;
    int sl;
    socklen_t len;

    struct sockaddr_in server,client;
    char buff[100];
    s=socket(AF_INET,SOCK_DGRAM,0);
    if(s==-1)
    {
        printf("\nSocket creation error");
        exit(0);
    }

    server.sin_family=AF_INET;
    server.sin_port=htons(8080);
    server.sin_addr.s_addr=inet_addr("127.0.0.1");
    sl=sizeof(server);
    len=sizeof(server);

    while(1)
    {
        printf("Enter file name \n");
        scanf("%s",buff);
        sntb=sendto(s,buff,sizeof(buff),0,(struct sockaddr *)&server, len);
        if(strcmp(buff,"exit")==0)
            break;
        recb=recvfrom(s,buff,sizeof(buff),0,(struct sockaddr *)&server,&sl);
        printf("%s",buff);
        if(strcmp(buff,"File Not Found")==0)
        {
            break;
        }
        else
        {
            printf("1.Search 2.Sort 3.Replace 4.Exit\n");
            int opt;
            scanf("%d",&opt);
        
            sprintf(buff,"%d",opt);
            sntb=sendto(s,buff,sizeof(buff),0,(struct sockaddr *)&server, len);

            if(opt==1)
            {
                printf("Enter string\n");
                scanf("%s",buff);
                sntb=sendto(s,buff,sizeof(buff),0,(struct sockaddr *)&server, len);
                recb=recvfrom(s,buff,sizeof(buff),0,(struct sockaddr *)&server,&sl);
                printf("count=%s\n",buff);

            }
            else if(opt==3)
            {
                printf("enter string\n");
                char buff1[50];
                scanf("%s",buff1);
                sntb=sendto(s,buff1,sizeof(buff1),0,(struct sockaddr *)&server, len);
                printf("enter replacement\n");
                char buff2[50];
                scanf("%s",buff2);
                sntb=sendto(s,buff2,sizeof(buff2),0,(struct sockaddr *)&server, len);
                recb=recvfrom(s,buff,sizeof(buff),0,(struct sockaddr *)&server,&sl);
                printf("%s\n",buff );
            }
            else if(opt==4)
            {
                break;
            }
        }

    }
}
