#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<stdlib.h>
#include<stdio.h>
void main()
{
	int s,r,recb,sntb,x,ns,a=0;
	socklen_t len;
	int ca;
	struct sockaddr_in server,client;
	char buff[500];
	char content[500];

	s=socket(AF_INET,SOCK_DGRAM,0);
	if(s==-1)
	{
		printf("\nSocket creation error");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8080);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	len=sizeof(client);
	ca=sizeof(client);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));

	if(r==-1)
	{
		printf("\nBinding error");
		exit(0);
	}

	while(1)
	{
		recb=recvfrom(s,buff,sizeof(buff),0,(struct sockaddr*)&client,&ca);
		printf("%s\n\n",buff);
		if(strcmp(buff,"stop")==0)
			exit(0);
		FILE *f;
		f=fopen(buff, "r");
		strcpy(buff,"");
		if(f==NULL)
		{
			strcpy(buff,"File Not Found");
			printf("\n");
			sntb=send(ns,buff,sizeof(buff),0);
		}
		else
		{
			fgets( content,1000,f);
			char temp[50];
			int i;
			int lines = 0;
			int alpha = 0;
			int digits=0;
			int spaces = 0;
			int other = -1;
			for(i=0;i<strlen(content)-1;i++)
			{
				if((content[i]>='a' && content[i]<='z') || (content[i]>='A' && content[i]<='Z'))
					alpha++;
				else if(content[i]>='0' && content[i]<='9')
					digits++;
				else if(content[i]==' ')
					spaces++;
				else if(content[i]=='\n')
					lines++;
				else
					other++;
			}
			int res = ftell(f);
			sprintf(temp,"\nSize=%dBytes\n",res);
			strcat(content,temp);
			sprintf(temp,"\nSpaces=%d\n",spaces);
			strcat(content,temp);
			sprintf(temp,"\nLines=%d\n",lines);
			strcat(content,temp);
			sprintf(temp,"\nAlphabets=%d\n",alpha);
			strcat(content,temp);
			sprintf(temp,"\nDigits=%d\n",digits);
			strcat(content,temp);
			sprintf(temp,"\nOther=%d\n",other);
			strcat(content,temp);
			sntb=sendto(s,content,sizeof(content),0,(struct sockaddr*)&client,len);
		}
	}
}
