#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<stdlib.h>
#include<stdio.h>
void main()
{
	int s,r,recb,sntb,x,ns,a=0;
	socklen_t len;
	struct sockaddr_in server,client;
	char buff[500];
	char content[500];
	s=socket(AF_INET,SOCK_STREAM,0);
	if(s==-1)
	{

		printf("\nSocket creation error.");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8080);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));
	if(r==-1)
	{
		printf("\nBinding error.");
		exit(0);
	}
	r=listen(s,1);
	if(r==-1)
	{
		close(s);
		exit(0);
	}

	len=sizeof(client);
	ns=accept(s,(struct sockaddr*)&client, &len);

	if(ns==-1)
	{
		close(s);
		exit(0);
	}
	while(1)
	{
		recb=recv(ns,buff,sizeof(buff),0);
		printf("%s\n\n",buff);
		if(strcmp(buff,"exit")==0)
			break;
		FILE *f;
		f=fopen(buff, "r");
		strcpy(buff,"");
		if(f==NULL)
		{
			strcpy(buff,"File Not Found");
			printf("\n");
			sntb=send(ns,buff,sizeof(buff),0);
		}
		else
		{
			fgets( content,1000,f);
			sntb=send(ns,content,sizeof(content),0);
			recb=recv(ns,buff,sizeof(buff),0);
			int opt=atoi(buff);
			if(opt==4)
			{
				break;
			}
			else if(opt==1)
			{
				recb=recv(ns,buff,sizeof(buff),0);
				printf("%s\n",buff);
				char temp[50];
				int le=strlen(content);
				int i;
				int l=0;
				int count=0;
				for(i=0;i<le;i++)
				{
					if(buff[i]==' '||buff[i]=='\0')
					{
						temp[l]='\0';
						l=0;
						if(strcmp(temp,buff)==0)
						{
							count++;
						}
						strcpy(temp,"");
					}
					else
					{
						temp[l++]=content[i];

					}
				}
				sprintf(buff,"%d",count);
				sntb=send(ns,buff,sizeof(buff),0);
			}
			else if(opt==2)
			{
			
				int j,i;
				for(i=0;i<strlen(content)-1;i++)
				{
					for(j=0;j<strlen(content)-i-1;j++)
					{
						if(content[j]>content[j+1])
						{
							char temp=content[j];
							content[j]=content[j+1];
							content[j+1]=temp;
						}
					}
				}
				printf("%s\n",content);
				fclose(f);
				f=fopen("file","w+");
				fputs(content,f);
			}
			else if(opt==3)
			{
				char buff1[50];
				recb=recv(ns,buff1,sizeof(buff1),0);
				printf("%s \n",buff1);
				char buff2[50];
				recb=recv(ns,buff2,sizeof(buff2),0);
				printf("%s \n",buff2);
				int le=strlen(content);
				int i;
				int l=0;
				char result[50];
				strcpy(result,"");
				char temp[50];
				int flag=0;
				for(i=0;i<le;i++)
				{

					if(content[i]==' '||content[i]=='\0')
					{
						temp[l]='\0';
						l=0;
						if(strcmp(temp,buff1)==0)
						{
							strcat(result," ");
							flag=1;
							strcat(result,buff2);
						}
						else
						{
							strcat(result," ");
							strcat(result,temp);	
						}
					}
					else
					{
						temp[l++]=content[i];
					}
				}
			
				if(flag==0)
				{
					strcpy(buff,"string not found \n");
				}
				else
				{
					strcpy(buff,"replacement done ");
					printf("\nthe new string is %s\n", result);
				}
				sntb=send(ns,buff,sizeof(buff),0);
			}
		}
	}
}
