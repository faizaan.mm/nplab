#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void main()
{
    int s,r,recb,sntb,x;
    int sl;
    socklen_t len;

    struct sockaddr_in server,client;
    char buff[100];
    s=socket(AF_INET,SOCK_DGRAM,0);
    if(s==-1)
    {
        printf("\nSocket creation error");
        exit(0);
    }

    server.sin_family=AF_INET;
    server.sin_port=htons(8080);
    server.sin_addr.s_addr=inet_addr("127.0.0.1");
    sl=sizeof(server);
    len=sizeof(server);

    while(1)
    {
        printf("Enter file name \n");
        scanf("%s",buff);
        sntb=sendto(s,buff,sizeof(buff),0,(struct sockaddr *)&server, len);
        if(strcmp(buff,"exit")==0)
            break;
        recb=recvfrom(s,buff,sizeof(buff),0,(struct sockaddr *)&server,&sl);
        printf("%s",buff);
        if(strcmp(buff,"File Not Found")==0)
        {
            break;
        }
    }
}
