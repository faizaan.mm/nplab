#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void main()
{
    int s,r,recb,sntb,x;

    struct sockaddr_in server;
    char buff[500];
    s=socket(AF_INET,SOCK_STREAM,0);
    if(s==-1)
    {
        printf("\nSocket creation error.");
        exit(0);
    }

    server.sin_family=AF_INET;
    server.sin_port=htons(8080);
    server.sin_addr.s_addr=inet_addr("127.0.0.1");

    r=connect(s,(struct sockaddr*)&server,sizeof(server));

    if(r==-1)
    {
        printf("\nConnection error.");
        exit(0);
    }

    while(1)
    {
        printf("Enter file name \n");
        scanf("%s",buff);
        sntb=send(s,buff,sizeof(buff),0);
        if(strcmp(buff,"exit")==0)
            break;
        recb=recv(s,buff,sizeof(buff),0);
        printf("%s",buff);
        if(strcmp(buff,"File Not Found")==0)
        {
            break;
        }
        else
        {
            printf("1.Search 2.Sort 3.Replace 4.Exit\n");
            int opt;
            scanf("%d",&opt);
        
            sprintf(buff,"%d",opt);
            sntb=send(s,buff,sizeof(buff),0);

            if(opt==1)
            {
                printf("Enter string\n");
                scanf("%s",buff);
                sntb=send(s,buff,sizeof(buff),0);
                recb=recv(s,buff,sizeof(buff),0);
                printf("count=%s\n",buff);

            }
            else if(opt==3)
            {
                printf("enter string\n");
                char buff1[50];
                scanf("%s",buff1);
                sntb=send(s,buff1,sizeof(buff1),0);
                printf("enter replacement\n");
                char buff2[50];
                scanf("%s",buff2);
                sntb=send(s,buff2,sizeof(buff2),0);
                recb=recv(s,buff,sizeof(buff),0);
                printf("%s\n",buff );
            }
            else if(opt==4)
            {
                break;
            }
        }

    }
}
