set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam &
	exit 0
}

set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]

$ns duplex-link $n0 $n4 2Mb 10ms DropTail
$ns duplex-link $n1 $n4 2Mb 10ms DropTail
$ns duplex-link $n2 $n4 2Mb 10ms DropTail
$ns duplex-link $n3 $n4 2Mb 10ms DropTail

$ns duplex-link-op $n0 $n4 orient right-up
$ns duplex-link-op $n1 $n4 orient left-up
$ns duplex-link-op $n2 $n4 orient right-down
$ns duplex-link-op $n3 $n4 orient left-down

$ns queue-limit $n0 $n4 5
$ns queue-limit $n1 $n4 5
$ns queue-limit $n2 $n4 5
$ns queue-limit $n3 $n4 5

set udp0 [new Agent/UDP]
$ns attach-agent $n0 $udp0
set udp1 [new Agent/UDP]
$ns attach-agent $n1 $udp1
set null0 [new Agent/Null]
$ns attach-agent $n2 $null0
set null1 [new Agent/Null]
$ns attach-agent $n3 $null1

set cbr0 [new Application/Traffic/CBR]
$cbr0 attach-agent $udp0
$cbr0 set packet_size_ 1000
$udp0 set packet_size_ 1000
$cbr0 set rate_ 1000000	 

set cbr1 [new Application/Traffic/CBR]
$cbr1 attach-agent $udp1
$cbr1 set packet_size_ 1000
$udp1 set packet_size_ 1000
$cbr1 set rate_ 1000000	 

$ns connect $udp0 $null1
$ns connect $udp1 $null0

$ns at 0 "$cbr0 start"
$ns at 5 "$cbr0 stop"
$ns at 5 "$cbr1 start"
$ns at 10 "$cbr1 stop"

$ns at 10 "finish"
$ns run