set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam &
	exit 0
}

$ns color 0 Blue
$ns color 1 Green 

set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]

$ns duplex-link $n0 $n1 1Mb 10ms DropTail
$ns duplex-link $n0 $n2 1Mb 10ms DropTail
$ns duplex-link $n2 $n3 1Mb 10ms DropTail
$ns duplex-link $n3 $n1 1Mb 10ms DropTail

$ns duplex-link-op $n0 $n1 orient right
$ns duplex-link-op $n0 $n2 orient down
$ns duplex-link-op $n2 $n3 orient right
$ns duplex-link-op $n3 $n1 orient up

$ns queue-limit $n0 $n1 5
$ns queue-limit $n2 $n3 5

set tcp0 [new Agent/TCP]
$ns attach-agent $n0 $tcp0
set tcp1 [new Agent/TCP]
$ns attach-agent $n2 $tcp1
set null0 [new Agent/TCPSink]
$ns attach-agent $n1 $null0
set null1 [new Agent/TCPSink]
$ns attach-agent $n3 $null1

set ftp0 [new Application/FTP]
$ftp0 attach-agent $tcp0
$ftp0 set type_ FTP 

set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1
$ftp1 set type_ FTP 

$tcp0 set fid_ 0
$tcp1 set fid_ 1

$ns connect $tcp0 $null0
$ns connect $tcp1 $null1

$ns at 0 "$ftp0 start"
$ns at 5 "$ftp0 stop"
$ns at 5 "$ftp1 start"
$ns rtmodel-at 5 down $n0 $n1
$ns at 10 "$ftp1 stop"

$ns at 10 "finish"
$ns run