set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam &
	exit 0
}

$ns color 0 Blue
$ns color 1 Green
$ns color 2 Yellow
$ns color 3 Magenta
$ns color 4 Brown 

set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]
set n6 [$ns node]
set n7 [$ns node]
set n8 [$ns node]
set n9 [$ns node]
set n10 [$ns node]
set n11 [$ns node]

$n0 color Blue
$n1 color Green
$n2 color Yellow
$n3 color Magenta
$n4 color Brown
$n5 color Violet
$n6 color Pink
$n7 color Orange
$n8 color Red
$n9 color Dodgerblue
$n10 color Darkgreen
$n11 color Black

$ns duplex-link $n2 $n0 2Mb 10ms DropTail
$ns duplex-link $n3 $n0 2Mb 10ms DropTail
$ns duplex-link $n4 $n0 2Mb 10ms DropTail
$ns duplex-link $n5 $n0 2Mb 10ms DropTail
$ns duplex-link $n6 $n0 2Mb 10ms DropTail
$ns duplex-link $n0 $n1 2Mb 10ms DropTail
$ns duplex-link $n7 $n1 2Mb 10ms DropTail
$ns duplex-link $n8 $n1 2Mb 10ms DropTail
$ns duplex-link $n9 $n1 2Mb 10ms DropTail
$ns duplex-link $n10 $n1 2Mb 10ms DropTail
$ns duplex-link $n11 $n1 2Mb 10ms DropTail

$ns duplex-link-op $n2 $n0 orient down
$ns duplex-link-op $n3 $n0 orient left-down
$ns duplex-link-op $n4 $n0 orient left
$ns duplex-link-op $n5 $n0 orient left-up
$ns duplex-link-op $n6 $n0 orient up
$ns duplex-link-op $n1 $n0 orient right
$ns duplex-link-op $n7 $n1 orient down
$ns duplex-link-op $n8 $n1 orient right-down
$ns duplex-link-op $n9 $n1 orient right
$ns duplex-link-op $n10 $n1 orient right-up
$ns duplex-link-op $n11 $n1 orient up

set tcp0 [new Agent/TCP]
$ns attach-agent $n7 $tcp0
set tcp1 [new Agent/TCP]
$ns attach-agent $n7 $tcp1
set tcp2 [new Agent/TCP]
$ns attach-agent $n7 $tcp2
set tcp3 [new Agent/TCP]
$ns attach-agent $n7 $tcp3
set tcp4 [new Agent/TCP]
$ns attach-agent $n7 $tcp4

set null0 [new Agent/TCPSink]
$ns attach-agent $n2 $null0
set null1 [new Agent/TCPSink]
$ns attach-agent $n3 $null1
set null2 [new Agent/TCPSink]
$ns attach-agent $n4 $null2
set null3 [new Agent/TCPSink]
$ns attach-agent $n5 $null3
set null4 [new Agent/TCPSink]
$ns attach-agent $n6 $null4

set ftp0 [new Application/FTP]
$ftp0 attach-agent $tcp0
$ftp0 set type_ FTP
set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1
$ftp1 set type_ FTP 
set ftp2 [new Application/FTP]
$ftp2 attach-agent $tcp2
$ftp2 set type_ FTP 
set ftp3 [new Application/FTP]
$ftp3 attach-agent $tcp3
$ftp3 set type_ FTP 
set ftp4 [new Application/FTP]
$ftp4 attach-agent $tcp4
$ftp4 set type_ FTP  

$tcp0 set fid_ 0
$tcp1 set fid_ 1
$tcp2 set fid_ 2
$tcp3 set fid_ 3
$tcp4 set fid_ 4

$ns connect $tcp0 $null0
$ns connect $tcp1 $null1
$ns connect $tcp2 $null2
$ns connect $tcp3 $null3
$ns connect $tcp4 $null4

$ns at 0 "$ftp0 start"
$ns at 5 "$ftp0 stop"
$ns at 5 "$ftp1 start"
$ns at 10 "$ftp1 stop"
$ns at 10 "$ftp2 start"
$ns at 15 "$ftp2 stop"
$ns at 15 "$ftp3 start"
$ns at 20 "$ftp3 stop"
$ns at 20 "$ftp4 start"
$ns at 25 "$ftp4 stop"

$ns at 25 "finish"
$ns run