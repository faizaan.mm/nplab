#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<string.h>
#define MAXSIZE 5000

void main()
{
int sockfd,retval;
int recedbytes,sentbytes;
struct sockaddr_in serveraddr;
char buff[MAXSIZE];
pid_t cpid;
sockfd=socket(AF_INET,SOCK_STREAM,0);
if(sockfd==-1)
{
printf("\nSocket Creation Error");

}

serveraddr.sin_family=AF_INET;
serveraddr.sin_port=htons(8080);
serveraddr.sin_addr.s_addr=inet_addr("127.0.0.1");
retval=connect(sockfd,(struct sockaddr*)&serveraddr,sizeof(serveraddr));
if(retval==-1)
{
printf("Connection error");

}
cpid=fork();
if(cpid==0)
{
	while(1)
	{
		char temp[16];
		scanf("%s",buff);
		strcat(buff,"\t-Client\n");
		sprintf(temp,"\tPID-%ld",(long)getpid());
		strcat(buff,temp);
		sentbytes=send(sockfd,buff,sizeof(buff),0);
		if(sentbytes==-1)
		{
			printf("!!");
			close(sockfd);
		}
	}
}
else
{
	while(1)
	{
		recedbytes=recv(sockfd,buff,sizeof(buff),0);
		puts(buff);
	}
	if(recedbytes==-1)
		{
			printf("!!");
			close(sockfd);
		}
}

close(sockfd);
}
