#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
void  main()
{
    int s,r,recb,sntb,x;
    char name[50], code[50];
    char regno[50];

    struct sockaddr_in server;
    char buff[55];

    s=socket(AF_INET,SOCK_STREAM,0);
    if(s==-1)
    {
        printf("\nSocket creation error");
        exit(0);
    }
    server.sin_family=AF_INET;
    server.sin_port=htons(8080);
    server.sin_addr.s_addr=inet_addr("127.0.0.1");
    r=connect(s,(struct sockaddr*)&server,sizeof(server));
    if(r==-1)
    {
        printf("\nConnection error");
        exit(0);
    }
    
    while(1)
    {
        printf("1.Name \n2.Regno \n3.Sub Code \n");
        int opt;    
        scanf("%d",&opt);
        sprintf(buff, "%d", opt);
        sntb=send(s,buff,sizeof(buff),0);
        if(opt==1)
        {
            printf("\nEnter name\n");   
            scanf("%s",name);
            sntb=send(s,name,sizeof(name),0);
        }
        else if(opt==2)
        {
            printf("\nEnter regno\n");   
            scanf("%s",regno);
            sntb=send(s,regno,sizeof(regno),0);
        }
        else if(opt==3)
        {
            printf("\nEnter subject code\n");   
            scanf("%s",code);
            sntb=send(s,code,sizeof(code),0);
        }
        recb = recv(s,buff,sizeof(buff),0);
        printf("%s \n",buff);
    }
}