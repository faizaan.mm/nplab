#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
struct student
{
	int regno;
	char name[50];
	char dept[50];
	char subcode[50];
	char section;
	int sem;
	char address[50];
	char courses[100];
	int marks;
} student[3];

void main()
{
	student[0].regno = 123;
	strcpy(student[0].name,"John");
	strcpy(student[0].dept,"ICT");
	strcpy(student[0].subcode,"ICT3008");
	student[0].section='A';
	student[0].sem = 6;
	strcpy(student[0].address,"123 Eashwar Nagar");
	strcpy(student[0].courses,"HSCN");
	student[0].marks = 25;

	student[1].regno = 124;
	strcpy(student[1].name,"Jake");
	strcpy(student[1].dept,"CSE");
	strcpy(student[1].subcode,"CSE3008");
	student[1].section='D';
	student[1].sem = 5;
	strcpy(student[1].address,"123 Madhav Nagar");
	strcpy(student[1].courses,"OS");
	student[1].marks = 75;

	student[2].regno = 126;
	strcpy(student[2].name,"Jack");
	strcpy(student[2].dept,"ICT");
	strcpy(student[2].subcode,"ICT4004");
	student[2].section='B';
	student[2].sem = 4;
	strcpy(student[2].address,"100 Eashwar Nagar");
	strcpy(student[2].courses,"DBS");
	student[2].marks = 90;

	int s,r,recb,sntb,x,ns,a=0,i=0;
	socklen_t len;
	struct sockaddr_in server,client;
	char buff[55];
	char name[50];
	char regno[50];
	char subcode[50];

	s=socket(AF_INET,SOCK_STREAM,0);
	if(s==-1)
	{

		printf("\nSocket creation error.");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8080);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));

	if(r==-1)
	{
		printf("\nBinding error.");
		exit(0);
	}

	r=listen(s,1);

	if(r==-1)
	{
		close(s);
		exit(0);
	}

	len=sizeof(client);
	ns=accept(s,(struct sockaddr*)&client, &len);
	if(ns==-1)
	{
		close(s);
		exit(0);
	}


	while(1)
	{
		recb=recv(ns,buff,sizeof(buff),0);
		int opt=atoi(buff);
		printf("%d\n",opt);
		if(opt==1)
		{	
			recb=recv(ns,name,sizeof(name),0);

			int pid = fork();
			
			if(pid==0)
			{
				for(i=0; i<3; i++)
				{
					if(strcmp(name,student[i].name)==0)
						break;
				}
				sprintf(buff,"Dept- %s, Sem - %d, Sec- %c, Courses - %s PID- %ld",student[i].dept,student[i].sem,student[i].section,student[i].courses,(long)getpid());
				sntb=send(ns,buff,sizeof(buff),0);
			}		
		}	
		else if(opt==2)
		{
			recb=recv(ns,regno,sizeof(regno),0);

			int pid = fork();
			
			if(pid==0)
			{
				for(i=0; i<3; i++)
				{
					if(atoi(regno) == student[i].regno)
						break;
				}
				sprintf(buff,"Name- %s, Address- %s PID-%ld",student[i].name,student[i].address,(long)getpid());
				sntb=send(ns,buff,sizeof(buff),0);
			}
		}
		else if(opt==3)
		{
			recb=recv(ns,subcode,sizeof(subcode),0);

			int pid = fork();
			
			if(pid==0)
			{
				for(i=0; i<3; i++)
				{
					if(strcmp(subcode,student[i].subcode)==0)
						break;
				}
				sprintf(buff,"Marks- %d PID-%ld",student[i].marks, (long)getpid());
				sntb=send(ns,buff,sizeof(buff),0);
			}
		}
		else
		{
			exit(0);
		}
	}
}