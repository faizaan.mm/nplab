#include<string.h>
#include<arpa/inet.h>
#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<errno.h>
#include<netdb.h>
void main()
{
	int s,r,recb,sntb,x,ns,a=0,i=0;
	socklen_t len;
	struct sockaddr_in server,client;
	char buff[50];
	struct hostent *hen;
	s=socket(AF_INET,SOCK_STREAM,0);
	if(s==-1)
	{

		printf("\nSocket creation error.");
		exit(0);
	}

	server.sin_family=AF_INET;
	server.sin_port=htons(8000);
	server.sin_addr.s_addr=htonl(INADDR_ANY);

	r=bind(s,(struct sockaddr*)&server,sizeof(server));

	if(r==-1)
	{
		printf("\nBinding error.");
		exit(0);
	}

	r=listen(s,1);

	if(r==-1)
	{
		close(s);
		exit(0);
	}

	len=sizeof(client);
	ns=accept(s,(struct sockaddr*)&client, &len);
	if(ns==-1)
	{
		close(s);
		exit(0);
	}

	recb=recv(ns,buff,sizeof(buff),0);
	hen=gethostbyname(buff);
	if(hen==NULL)
	{
		sprintf(buff,"Host not found \n");
	}
	else
	{
		sprintf(buff,"%s\n",inet_ntoa(*((struct in_addr *)hen->h_addr)));
	}
	sntb=send(ns,buff,sizeof(buff),0);
}